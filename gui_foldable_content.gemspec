$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "gui_foldable_content/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "gui_foldable_content"
  s.version     = GuiFoldableContent::VERSION
  s.authors     = ["Leo Benkel"]
  s.email       = ["leo.benkel@gmail.com"]
  s.homepage    = "https://gitlab.com/wonay/gui_foldable_content"
  s.summary     = "Helper to have foldable blocks of html"
  s.description = "Helper to have foldable blocks of html"
  s.license     = "MIT"

  s.files = Dir["{app,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.required_ruby_version = ">= 2.3.0"
  s.add_dependency "rails", ">= 4.2.4"
  s.add_dependency 'jquery-rails', '> 3.0.0'
end
