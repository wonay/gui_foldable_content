module GuiFoldableContent
	module ViewHelpers
		def create_foldable_area(title: , title_tag: "h3", default_state: false, close_indicator: GuiFoldableContent.configuration.default_close_indicator, open_indicator: GuiFoldableContent.configuration.default_open_indicator )

			render partial: 'gui_foldable_content/foldable_content', locals: { _title_tag: title_tag, _title: title, _default_state: default_state, _content: capture { yield }, _close_indicator: simple_format(close_indicator, {class: 'gui_foldable_indicator_close'}, wrapper_tag: "span" ) , _open_indicator: simple_format(open_indicator, { class: 'gui_foldable_indicator_open'}, wrapper_tag: "span")  }
		end
	end
end

ActionView::Base.send :include, GuiFoldableContent::ViewHelpers
